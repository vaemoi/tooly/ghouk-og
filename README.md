# ghouk-og

Branch dependent git hooks. Active development [here](https://gitlab.com/vaemoi/tooly/ghouk)

Deprecated. Use `npm install @vaemoi/ghouk` instead
